import React from 'react';
import styles from './Logo.module.scss';

export const Logo = () => {
  return (
    <a href="#" className={styles.LogoLink}>
      <img src="./img/logo1.png" alt="Logo" />
    </a>
  );
};
